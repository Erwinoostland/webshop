@extends('layouts.app')

@section('content')
    <section class="container">
        <section class="row mb-4">
            @foreach ($cars as $car)
                <section class="col-md-4">
                    <section class="card">
                        <img class="card-img-top"
                             src="{{$car->img}}">
                        <section class="card-body">
                            <h5>{{$car->name}}</h5>
                            <p>{{$car->description}}</p>
                            <p>€{{$car->price}}</p>
                        </section>
                        <section class="card-body">
                            <a href="{{ url('cardetails/'. $car->id )}}" class="btn btn-primary">Details</a>
                        </section>
                    </section>
                </section>
            @endforeach
        </section>
    </section>
@endsection
