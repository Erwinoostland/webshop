@extends ('layouts.app')

@section('content')


    <section class="container">
        <section class="row">
            <section class="col-md-12">
                <section class="card card-contact">
                    <section class="card-header">
                        <h4 class="user-h4-title">Contact</h4>
                    </section>

                    <form method="post" action="{{ url('/contact/store') }}">

                        {{ csrf_field() }}
                        @if(\Session::has('sessionMessage'))
                            <section
                                    class="{{\Session::get('sessionClass')}}">{{\Session::get('sessionMessage')}}</section>
                        @endif

                        <section class="card-body">
                            <section class="row">
                                <section class="col-md-12">
                                    <label for="title">Titel:
                                        <input type="text" id="title" name="title" class="edit-user-input" required>
                                    </label>
                                </section>
                            </section>
                            <section class="form-group">
                                <label for="description">Description:</label>
                                <textarea class="form-control" rows="5" name="description" id="description"
                                          required></textarea>
                            </section>
                            <button type="submit" name="submit" class="basic-button">Verstuur</button>
                        </section>
                    </form>
                </section>
            </section>
        </section>
    </section>

@endsection