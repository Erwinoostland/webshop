@extends('layouts.app')

@section('content')

    <section class="container">
        <section class="row">
            <section class="col-md-12">
                <section class="card card-profile">
                    <section class="card-header">
                        <h4 class="user-h4-title">Profiel</h4>
                    </section>
                    <section class="card-body">

                        @if(\Session::has('sessionMessage'))
                            <section
                                    class="{{\Session::get('sessionClass')}}">{{\Session::get('sessionMessage')}}</section>
                        @endif
                        {{-- name --}}
                        <section class="row">
                            <section class="col-md-12">
                                <label for="name">Naam:
                                    <input type="text" id="name" name="name" class="edit-user-input"
                                           value="{{$user->name}}" disabled>
                                </label>
                            </section>
                        </section>

                        {{-- email --}}
                        <section class="row">
                            <section class="col-md-12">
                                <label for="email">Email:
                                    <input type="text" id="email" name="email" class="edit-user-input"
                                           value="{{$user->email}}" disabled>
                                </label>
                            </section>
                        </section>

                        <a href="" data-target="#edit-modal" data-toggle="modal"
                           class="basic-button">Wijzig</a>
                        <hr/>

                        <section class="card-header">
                            <h4 class="user-h4-title">Aankoop geschiedenis</h4>
                        </section>
                        <table class="table table-striped table-responsive">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Naam</th>
                                <th>Prijs</th>
                                <th>Gekocht op</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </section>
                </section>
            </section>
        </section>
    </section>
    </section>


    {{--Edit Modal--}}
    <section class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="edit-modal"
             aria-hidden="true">
        <section class="modal-dialog" role="document">
            <section class="modal-content">
                <section class="modal-header">
                    <h5 class="modal-title" id="edit-modal"> Persoongegevens Wijzigen</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </section>

                <form method="post" action="{{ url('/edit/'. $user->id) }}" enctype="multipart/form-data">

                    {{ csrf_field() }}

                    <section class="modal-body">

                        {{-- Name--}}
                        <label for="name" class="col-md-12">Naam:
                            <input type="text" name="name" id="name" class="edit-input"
                                   value="{{$user->name}}">
                        </label>

                        {{-- Email --}}
                        <label for="email" class="col-md-12">Email:
                            <input type="text" name="email" id=email" class="edit-input"
                                   value="{{$user->email}}">
                        </label>

                        {{-- Password --}}
                        <label for="password" class="col-md-12">Wachtwoord:
                            <input type="password" name="password" id="password" class="edit-input"
                                   placeholder="******" required>
                        </label>

                        {{-- Confirm Password --}}
                        <label for="confirmation_password" class="col-md-12">Herhaal Wachtwoord:
                            <input type="password" name="confirmation_password" id="confirmation_password"
                                   class="edit-user__input" placeholder="******" required>
                        </label>


                    </section>

                    <section class="modal-footer">
                        <button class="basic-button" data-dismiss="modal">Sluiten</button>
                        <button type="submit" class="basic-button">Opslaan</button>
                    </section>
                </form>
            </section>
        </section>
    </section>

@endsection