@extends ('layouts.app')

@section('content')
    <section class="container">
        <section class="row">
            <section class="col-md-12">
                <section class="card card-car">
                    <section class="card-body">
                        <form method="post" action="{{ url('/cardetails/store/'. $car->id) }}">

                            {{ csrf_field() }}
                            @if(\Session::has('sessionMessage'))
                                <section
                                        class="{{\Session::get('sessionClass')}}">{{\Session::get('sessionMessage')}}</section>
                            @endif
                            <section class="row">

                                <section class="col-md-4">
                                    <img class="card-img-top" src="{{$car->img}}" width="400px">
                                </section>
                                <section class="col-md-6">
                                    <h4 class="user-h4-title">{{$car->name}}</h4>
                                    <hr/>
                                    <p>{{$car->description}}</p>
                                    <p>€{{$car->price}}</p>
                                    <button class="basic-button">Kopen</button>
                                </section>
                            </section>
                        </form>
                    </section>
                </section>
            </section>
        </section>
    </section>


@endsection