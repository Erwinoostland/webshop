<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Auth;
use App\Contact;

class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('contact');
    }

    public function store(Request $request) {

        $newContact = new Contact();

        $newContact->title = $request->input('title');
        $newContact->description = $request->input('description');


        $newContact->userID= \Auth::id();

        if ($newContact->save()){
            $sessionClass = "alert alert-success";
            $sessionMessage = "Uw contact aanvraag is verzonden!";
        } else {
            $sessionClass = "alert alert-danger";
            $sessionMessage = "Er is iets mis gegaan!";
            return back()->with(['sessionClass' => $sessionClass, 'sessionMessage' => $sessionMessage]);
        }

        return back()->with(['sessionClass' => $sessionClass, 'sessionMessage' => $sessionMessage]);
    }
}
