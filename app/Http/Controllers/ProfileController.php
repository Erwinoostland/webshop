<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Car;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = User::find(\Auth::id());
        $cars = Car::limit(5)->orderBy('created_at', 'DESC')->get();
        return view('profile', compact('user', 'cars'));
    }

    public function edit(Request $request, $id) {

        $editUser = User::find($id);
        $editUser->name = $request->input('name');
        $editUser->email = $request->input('email');
        $editUser->password = bcrypt($request->input('password'));

        $editUser->update();

        if ($editUser->update()){
            $sessionClass = "alert alert-success";
            $sessionMessage = "U heeft uw profiel gewijzigd!";
        } else {
            $sessionClass = "alert alert-danger";
            $sessionMessage = "Er is iets mis gegaan!";
            return back()->with(['sessionClass' => $sessionClass, 'sessionMessage' => $sessionMessage]);
        }

        return back()->with(['sessionClass' => $sessionClass, 'sessionMessage' => $sessionMessage]);
    }
}
