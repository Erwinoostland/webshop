<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Car;
use App\Order;

class CardetailsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
        $car = Car::find($id);
        return view('cardetails', compact('car'));
    }

    public function store($id){

        $car = Car::find($id);

        $newOrder = new Order();
        $newOrder->userID = \Auth::id();
        $newOrder->carID = $id;
        $newOrder->price = $car->price;
        $newOrder->status = "verzonden";

        $newOrder->save();

        if ($newOrder->save()){
            $sessionClass = "alert alert-success";
            $sessionMessage = "Bedankt voor uw interesse!";
        } else {
            $sessionClass = "alert alert-danger";
            $sessionMessage = "Er is iets mis gegaan!";
            return back()->with(['sessionClass' => $sessionClass, 'sessionMessage' => $sessionMessage]);
        }

        return back()->with(['sessionClass' => $sessionClass, 'sessionMessage' => $sessionMessage]);


    }
}
