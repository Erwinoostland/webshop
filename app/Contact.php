<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Contact extends Model
{
    public function contact()
    {
        return $this->hasOne("App\User", "id", "userID");
    }
}
